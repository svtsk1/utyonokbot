# - *- coding: utf- 8 - *-
import telebot
import parser
import sys
from importlib import reload
reload(sys)
sys.setdefaultencoding('utf-8')

bot = telebot.TeleBot('')


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAIBE2AWwHFH9Ppnxo8EtWtnc2JhKKqZAAIEAQACVp29Ct4E0XpmZvdsHgQ')


@bot.message_handler(commands=['check'])
def send_reviews(message):
    currency = parser.check()
    for i in range(1, len(currency)):
        bot.send_message(message.chat.id, f'{currency[i][0]}: buy USD: {currency[i][1]}, sell USD: {currency[i][2]}, buy EUR: {currency[i][7]}, sell EUR: {currency[i][8]}')


bot.polling(none_stop=True)
