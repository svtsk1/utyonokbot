import requests
from bs4 import BeautifulSoup

url = 'https://myfin.by/currency/minsk'
headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0'}


def check():
    full_page = requests.get(url, headers=headers)
    text = full_page.text
    soup = BeautifulSoup(text, features="html.parser")
    data = []
    for i in range(1, 30):
        rows = soup.find_all("tr", {"class": f"tr-tb acc-link_{i} not_h"})
        if len(rows) != 0:
            for row in rows:
                cols = row.find_all('td')
                cols = [element.text.strip() for element in cols]
                data.append([element for element in cols if element])
    return data

